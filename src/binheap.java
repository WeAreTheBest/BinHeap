// Binomial Heap mit Schlüsseltyp K (der die Schnittstelle Comparable<K>
// oder Comparable<K'> für einen Obertyp K' von K implementieren muss)
// und Werttyp V.
class BinHeap <K extends Comparable<? super K>, V> {
    // Heap-Eintrag, bestehend aus einem Schlüssel (Priorität) key
    // mit Typ K und einem Wert (Satellitendaten) val mit Typ V.
    // Wenn der Eintrag momentan zu einem Heap gehört, verweist node
    // auf den zugehörigen Knoten eines Binomialbaums dieses Heaps.
    
    public static class Entry <K, V> {
	// Schlüssel, Wert und zugehöriger Knoten.
	private K key;
	private V val;
	private Node<K, V> node;

	// Heap-Eintrag mit Schlüssel k und Wert v erzeugen.
	private Entry (K k, V v) {
	    key = k;
	    val = v;
	}

	// Schlüssel bzw. Wert liefern.
	public K key () { return key; }
	public V val () { return val; }
    }

    // Knoten eines Binomialbaums in einem BinHeap<K, V>.
    // Neben den eigentlichen Knotendaten (degree, parent, child, sibling),
    // enthält der Knoten einen Verweis auf den zugehörigen Heap-Eintrag.
    private static class Node <K, V> {
        private BinHeap binheap;
	// Zugehöriger Heap-Eintrag.
	private Entry<K, V> entry;

	// Grad des Knotens.
	private int degree;

	// Zeiger auf Vater-, ersten Kind- und Geschwisterknoten.
	private Node<K, V> parent, child, sibling;

	// Knoten erzeugen, der auf den Heap-Eintrag e verweist und umgekehrt.
	private Node (BinHeap b, Entry<K, V> e) {
            binheap = b;
	    entry = e;
	    e.node = this;
	}

	// Schlüssel des Knotens, d. h. des zugehörigen Heap-Eintrags liefern.
	private K key () { return entry.key; }
    }

    private int size;
    private Node<K,V> head;
    
    public BinHeap()
    {
    }
    
    private void link(Node<K,V> n1, Node<K,V> n2)
    {
        n1.parent = n2;
        n1.sibling = n2.child;
        n2.child = n1;
        n2.degree++;
    }
    
    private void merge(BinHeap bh)
    {
        Node<K,V> h = null,x = null,y,n1 = this.head,n2 = bh.head;
        while((n1 != null) & (n2 != null))
        {
            if(n1.degree < n2.degree)
            {
                y = n1;
                n1 = n1.sibling;
            }
            else
            {
                y = n2;
                n2 = n2.sibling;
            }
            if(h == null)
            {
                h = y;
            }
            else
            {
                x.sibling = y;
            }
            x = y;
        }
        if(n1 != null)
        {
            if(h == null)
            {
                h = n1;
            }
            else
            {
                x.sibling = n1;
            }
        }
        if(n2 != null)
        {
            if(h == null)
            {
                h = n2;
            }
            else
            {
                x.sibling = n2;
            }
        }
        head = h;
    }
    
    private void union(BinHeap bh)
    {
        Node<K,V> x_prev,x,x_next;
        
        merge(bh);
        x_prev = null;
        x = head;
        x_next = x.sibling;
        while(x_next != null)
        {
            if((x.degree != x_next.degree) | ((x_next.sibling != null ? x_next.sibling.degree == x.degree : false)))
            {
                x_prev = x;
                x = x_next;
            }
            else if(x.key().compareTo(x_next.key()) <= 0)
            {
                x.sibling = x_next.sibling;
                link(x_next, x);
            }
            else
            {
                if(x_prev == null)
                {
                    head = x_next;
                }
                else
                {
                    x_prev.sibling = x_next;
                }
                link(x, x_next);
                x = x_next;
            }
            x_next = x.sibling;
        }
    }
    
    private Node<K,V> invert(Node<K,V> x)
    {
        Node<K,V> h = null, cur = x.child, next;
        x.child = null;
        while(cur != null)
        {
            next = cur.sibling;
            cur.parent = null;
            cur.sibling = h;
            h = cur;
            cur = next;
        }
        return h;
    }
    
    public int size()
    {
        return size;
    }
    
    public boolean isEmpty()
    {
        return ((size()==0) | (head == null));
    }
    
    public boolean contains(Entry<K,V> e)
    {
        if(e != null)
        {
            if(e.node != null)
            {
                if(e.node.binheap != null)
                {
                    return e.node.binheap.equals(this);
                }
            }
        }
        return false;
    }
    
    public Entry<K,V> insert(K k, V v)
    {
        Entry<K,V> e;
        Node<K,V> n;
        if(k == null)
        {
            return null;
        }
        e = new Entry(k, v);
        n = new Node(this, e);
        BinHeap bh = new BinHeap();
        bh.head = n;
        union(bh);
        size++;
        return e;
    }
    
    public boolean decreaseKey(Entry<K,V> e, K k)
    {
        if(contains(e) & (e.key().compareTo(k) > 0))
        {
            e.key = k;
            Entry<K,V> swap;
            Node<K,V> n1,n2;
            n1 = e.node;
            n2 = n1.parent;
            while((n2 != null) & (n1.key().compareTo(n2.key()) < 0))
            {
                swap = n1.entry;
                n1.entry = n2.entry;
                n2.entry = swap;
                n1.entry.node = n1;
                n2.entry.node = n2;
                n1 = n2;
                n2 = n1.parent;
            }
            return true;
        }
        return false;
    }
    
    public Entry<K,V> minimum()
    {
        if(!isEmpty())
        {
            Node<K,V> r = head,n = r.sibling;
            while(n != null)
            {
                if(n.key().compareTo(r.key()) < 0)
                {
                    r = n;
                }
                n = n.sibling;
            }
            return r.entry;
        }
        return null;
    }
    
    public Entry<K,V> extractMin()
    {
        if(!isEmpty())
        {
            Node<K,V> r = head,n = r.sibling,pn = r,pr = null;
            while(n != null)
            {
                if(n.key().compareTo(r.key()) < 0)
                {
                    r = n;
                    pr = pn;
                }
                pn = n;
                n = n.sibling;
            }
            if(pr != null)
            {
                pr.sibling = r.sibling;
            }
            else
            {
                head = r.sibling;
            }
            r.sibling = null;
            if(r.child != null)
            {
                Node<K,V> h = invert(r);
                BinHeap<K,V> bh = new BinHeap();
                bh.head = h;
                union(bh);
            }
            size--;
            return r.entry;
        }
        return null;
    }
    
    public boolean delete(Entry<K,V> e)
    {
        if((!isEmpty()) & contains(e))
        {
            Node<K,V> y = e.node,z = y.parent;
            Entry<K,V> swap;
            while(z != null)
            {
                swap = z.entry;
                z.entry = y.entry;
                y.entry = swap;
                z.entry.node = z;
                y.entry.node = y;
                y = z;
                z = y.parent;
            }
            y = head;
            z = null;
            while(y != e.node)
            {
                z = y;
                y = y.sibling;
            }
            if(z != null)
            {
                z.sibling = y.sibling;
            }
            else
            {
                head = y.sibling;
            }
            y.sibling = null;
            if(y.child != null)
            {
                Node<K,V> h = invert(y);
                BinHeap<K,V> bh = new BinHeap();
                bh.head = h;
                union(bh);
            }
            size--;
            return true;
        }
        return false;
    }
    
    void dump()
    {
        Node<K,V> n = head;
        int padding = 0;
        while(n != null)
        {
            for(int i = 0; i < padding; i++)
            {
                System.out.print("  ");
            }
            System.out.print(n.entry.key);
            System.out.print(" ");
            System.out.println(n.entry.val);
            if(n.child != null)
            {
                n = n.child;
                padding++;
            }
            else if(n.sibling != null)
            {
                n = n.sibling;
            }
            else
            {
                n = n.parent;
                padding--;
                while(n != null)
                {
                    if(n.sibling != null)
                    {
                        n = n.sibling;
                        break;
                    }
                    n = n.parent;
                    padding--;
                }
            }
        }
    }
}

// Interaktives Testprogramm für die Klasse BinHeap.
class BinHeapTest {
    public static void main (String [] args) throws java.io.IOException {
	// Leeren Binomial Heap erzeugen.
	BinHeap<Integer, String> heap = new BinHeap<Integer, String>();

	// Array mit allen eingefügten Heap-Einträgen, damit sie später
	// für delete und decreaseKey verwendet werden können.
	// Achtung: Obwohl die Klasse BinHeap ebenfalls Typparameter
	// besitzt, schreibt man "BinHeap.Entry<Integer, String>" und
	// nicht "BinHeap<Integer, String>.Entry<Integer, String>".
	// Achtung: "new BinHeap.Entry [100]" führt zu einem Hinweis
	// über "unchecked or unsafe operations"; die eigentlich "korrekte"
	// Formulierung "new BinHeap.Entry<Integer, String> [100]"
	// führt jedoch zu einem Übersetzungsfehler!
	BinHeap.Entry<Integer, String> [] entrys = new BinHeap.Entry [100];

	// Anzahl der bisher eingefügten Heap-Einträge.
	int n = 0;

	// Standardeingabestrom System.in als InputStreamReader
	// und diesen wiederum als BufferedReader "verpacken".
	java.io.BufferedReader r = new java.io.BufferedReader(
	  new java.io.InputStreamReader(System.in));

	// Endlosschleife.
	while (true) {
	    // Inhalt und Größe des Heaps ausgeben.
	    heap.dump();
	    System.out.println(heap.size() + " entry(s)");

	    // Eingabezeile vom Benutzer lesen, ggf. ausgeben (wenn das
	    // Programm nicht interaktiv verwendet wird) und in einzelne
	    // Wörter zerlegen.
	    // Abbruch bei Ende der Eingabe oder leerer Eingabezeile.
	    System.out.print(">>> ");
	    String line = r.readLine();
	    if (line == null || line.equals("")) return;
	    if (System.console() == null) System.out.println(line);
	    String [] cmd = line.split(" ");

	    // Fallunterscheidung anhand des ersten Zeichens
	    // des ersten Worts.
	    switch (cmd[0].charAt(0)) {
	    case '+': // insert key val
		entrys[n] = heap.insert(Integer.parseInt(cmd[1]), cmd[2]);
		System.out.println("entry " + n);
		n++;
		break;
	    case '-': // delete entry
		heap.delete(entrys[Integer.parseInt(cmd[1])]);
		break;
	    case '?': // minimum
		BinHeap.Entry<Integer, String> e = heap.minimum();
		System.out.println(e.key() + " " + e.val());
		break;
	    case '!': // extractMin
		e = heap.extractMin();
		System.out.println(e.key() + " " + e.val());
		break;
	    case '<': // decreaseKey entry key
		heap.decreaseKey(entrys[Integer.parseInt(cmd[1])],
		  Integer.parseInt(cmd[2]));
		break;
	    }
	}
    }
}
